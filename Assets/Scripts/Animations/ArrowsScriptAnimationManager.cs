using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowsScriptAnimationManager : MonoBehaviour
{
    public GameObject[] arrows;
    int i = 0;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("enableArrows",0f);
        Invoke("enableArrows",0.3f);
        Invoke("enableArrows",0.6f);
    }

    void enableArrows(){
        arrows[i].SetActive(true);
        i++;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
