using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplotionsController : MonoBehaviour
{
    int numberOfExplotions = 0;
    public int maxExplotions = 8;
    public int minExplotions = 3;
    public float randomPosition = 0.3f;
    public float minScale = 0.1f;
    public float maxScale = 1f;
    public GameObject explotionPrefab;
    public AudioClip[] audios;
    // Start is called before the first frame update
    void Start()
    {
        numberOfExplotions = Random.Range(minExplotions,maxExplotions);
        StartCoroutine(go());
        Invoke("autoDestroy",3f);
    }

    void autoDestroy(){
        Destroy(this.gameObject);
    }

    IEnumerator go()
    {    
        for(int i = 0; i<numberOfExplotions; i++){           
            yield return new WaitForSeconds(Random.Range(0,0.2f));
            Instantiate();
        }
    }
    void Instantiate(){
        GameObject go = Instantiate(explotionPrefab, new Vector3(this.transform.position.x,this.transform.position.y, this.transform.position.z), Quaternion.identity);
        go.transform.parent = this.transform;
        go.transform.position = new Vector3(this.transform.position.x+Random.Range(0,randomPosition),this.transform.position.y,this.transform.position.z + Random.Range(0,randomPosition));
        go.transform.eulerAngles = new Vector3(
            go.transform.eulerAngles.x+90,
            go.transform.eulerAngles.y,
            go.transform.eulerAngles.z
        );       
        go.GetComponent<AudioSource>().clip = audios[Random.Range(0,audios.Length-1)];
        go.GetComponent<AudioSource>().enabled = true;
        float aux = Random.Range(minScale,maxScale);
        go.transform.localScale = new Vector3(aux, aux, aux);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
