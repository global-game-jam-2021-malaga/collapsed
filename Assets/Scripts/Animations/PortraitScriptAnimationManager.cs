using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortraitScriptAnimationManager : MonoBehaviour
{
    
    public Sprite sprite1;
    public Sprite sprite2;
    private Image image;
    private bool val = false;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        StartCoroutine(go());
    }

    IEnumerator go()
    { 
        while(true){ 
            
            yield return new WaitForSeconds(Random.Range(1,5));
            if(val){          
                image.sprite = sprite1;
            }else{
                image.sprite = sprite2;
            }
            val = !val;  
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
