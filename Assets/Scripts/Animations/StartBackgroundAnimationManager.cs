using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBackgroundAnimationManager : MonoBehaviour
{
    public GameObject Layer1;
    public GameObject Layer2;
    public GameObject Layer3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Layer1.transform.Translate(-Vector3.right * 0.1f * Time.deltaTime);
        Layer2.transform.Translate(-Vector3.right * 0.18f * Time.deltaTime);
        Layer3.transform.Translate(-Vector3.right * 0.23f * Time.deltaTime);
    }
}
