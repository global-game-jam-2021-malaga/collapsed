using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorButton : MonoBehaviour
{
    int life = 5;
    public float randomPosition = 5f;
    public float explotionMinScale = 5f;
    public float explotionMaxScale = 10f;
    public GameObject explotionPrefab;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void Test(){
        addDamage();
        addDamage();
        addDamage();
        addDamage();
        addDamage();
    }

    private void addDamage(){
        life--;
        if(life==0){
            transform.parent.GetComponent<DoorManager>().removeButton();
            GameObject go = Instantiate(explotionPrefab, new Vector3(this.transform.position.x,this.transform.position.y, this.transform.position.z), Quaternion.identity);
            go.GetComponent<ExplotionsController>().minScale = explotionMinScale;
            go.GetComponent<ExplotionsController>().maxScale = explotionMaxScale;
            go.GetComponent<ExplotionsController>().randomPosition = randomPosition;
            GetComponent<Collider>().enabled=false;
            Invoke("autoDestroy",0.2f);
        }
    }

    private void autoDestroy(){
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        addDamage();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
