using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    public GameObject door1;
    public GameObject door1To;
    public GameObject door2;
    public GameObject door2To;
    private bool play = false;

    private int buttons = 2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void open(){
        play = true;
    }

    public void removeButton(){
        buttons--;
        if(buttons==0){
            Invoke("open",1f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(play){
            door1.transform.position = Vector3.MoveTowards(door1.transform.position, door1To.transform.position, 0.3f);
            door2.transform.position = Vector3.MoveTowards(door2.transform.position, door2To.transform.position, 0.3f);
        }
    }
}
