using System;
using System.Collections.Generic;
using UnityEngine;

namespace Collapsed.Level {
    
    public class Chunk : MonoBehaviour {

        public GameObject endPoint;
        public List<Transform> spawnPoints;

        private Renderer[] renderers = new Renderer[0];
        private bool spawnedPointsObtained;
        private List<Transform> spawnPointsEmpty = new List<Transform>();
        
        public bool IsVisible() {
            if (renderers.Length == 0) return true;
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
            foreach (Renderer renderer in renderers) {
                if (GeometryUtility.TestPlanesAABB(planes, renderer.bounds)) {
                    return true;
                }
            }
            return false;
        }

        public List<Transform> GetSpawnPoints() {
            if (!spawnedPointsObtained) {
                spawnedPointsObtained = true;
                return spawnPoints;
            }
            return spawnPointsEmpty;
        }

        private void Start() {
            renderers = GetComponentsInChildren<Renderer>();
        }
    }
}