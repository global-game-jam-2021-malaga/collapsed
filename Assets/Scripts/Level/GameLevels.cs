using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Collapsed.Level {
    
    public class GameLevels : MonoBehaviour {

        [SerializeField]
        private List<LevelConfiguration> levels = new List<LevelConfiguration>();

        private Level currentLevel;

        public void GenerateLevel() {
            if (currentLevel != null) {
                DestroyImmediate(currentLevel);
            }
            LevelConfiguration levelConfiguration = levels[Random.Range(0, levels.Count)];
            Level.Create(gameObject, levelConfiguration);
        }

        private void Start() {
            GenerateLevel();
        }
    }
}