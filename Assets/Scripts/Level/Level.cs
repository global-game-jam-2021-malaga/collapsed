using System;
using System.Collections.Generic;
using FlowCanvas.Nodes;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Collapsed.Level {
    
    public abstract class Level : MonoBehaviour {
        
        [SerializeField] 
        protected LevelConfiguration levelConfiguration;

        protected Chunk firstChunk;
        protected Chunk secondChunk;
        protected Chunk thirdChunk;

        protected uint numberOfRegularChunks;
        protected uint numberOfChunks;
        protected Vector3 nextChunkPosition = new Vector3(-20, 0, 0);
        protected bool levelFinished;

        public static Level Create(GameObject gameObject, LevelConfiguration levelConfiguration) {
            Level newLevel = (Level) gameObject.AddComponent(Type.GetType(levelConfiguration.levelType));
            newLevel.levelConfiguration = levelConfiguration;
            return newLevel;
        }
        
        protected abstract Chunk GetRegularChunk(); 
        protected abstract Chunk GetDirectionalChunk(); 
        protected abstract List<KeyValuePair<GameObject, Transform>> GetEnemies();

        protected virtual void InitializeChunks() {
            firstChunk = InstantiateChunk(levelConfiguration.firstChunk.GetComponent<Chunk>());
            secondChunk = InstantiateChunk(GetRegularChunk());
            thirdChunk = InstantiateChunk(GetRegularChunk());
            numberOfRegularChunks = 3;
        }

        protected virtual void GenerateChunks() {
            if (!levelFinished && !firstChunk.IsVisible()) {
                Chunk chunkToDelete = firstChunk;
                firstChunk = secondChunk;
                secondChunk = thirdChunk;
                if (numberOfChunks < levelConfiguration.numberOfChunksToWin) {
                    thirdChunk = InstantiateChunk(CreateDirectionalChunk()
                        ? GetDirectionalChunk()
                        : GetRegularChunk());
                }
                else {
                    levelFinished = true;
                    thirdChunk = InstantiateChunk(levelConfiguration.finalChunk.GetComponent<Chunk>());
                }
                DestroyChunk(chunkToDelete); 
                Master.Instance.addScore(100);
            }
        }

        protected virtual void GenerateEnemies() {
            List<KeyValuePair<GameObject, Transform>> enemiesToSpawn = GetEnemies();
            foreach (KeyValuePair<GameObject, Transform> enemyToSpawn in enemiesToSpawn) {
                Instantiate(enemyToSpawn.Key, enemyToSpawn.Value.position, enemyToSpawn.Value.rotation);
            }
        }

        protected Chunk InstantiateChunk(Chunk chunk) {
            Chunk newChunk = Instantiate(chunk.gameObject).GetComponent<Chunk>();
            newChunk.transform.position = nextChunkPosition;
            nextChunkPosition = newChunk.endPoint.transform.position;
            ++numberOfChunks;
            return newChunk;
        }
        
        protected void DestroyChunk(Chunk chunk) {
            DestroyImmediate(chunk.gameObject);
        }

        protected virtual bool CreateDirectionalChunk() {
            if (numberOfRegularChunks > levelConfiguration.maximumFollowedRegularChunks) {
                return true;
            }
            return numberOfRegularChunks >= levelConfiguration.minimumFollowedRegularChunks &&
                   numberOfRegularChunks <= levelConfiguration.maximumFollowedRegularChunks &&
                   Random.value > .5f;
        }

        protected virtual void Start() {
            InitializeChunks();
        }

        protected virtual void Update() {
            GenerateChunks();
            GenerateEnemies();
        }        
    }
}