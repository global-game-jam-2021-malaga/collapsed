using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;
#if UNITY_EDITOR
using System;
using UnityEditor;
#endif

namespace Collapsed.Level {
    
    [CreateAssetMenu(fileName = "LevelConfiguration", menuName = "Collapsed/LevelConfiguration", order = 1)]
    public class LevelConfiguration : ScriptableObject {
        
        public string id;

        [Header("Restrictions")] 
        public uint minimumFollowedRegularChunks;
        public uint maximumFollowedRegularChunks;
        public uint numberOfChunksToWin;
        
        [Header("Assets")]
        public List<GameObject> enemies = new List<GameObject>();
        public GameObject firstChunk;
        public GameObject finalChunk;
        public List<GameObject> regularChunks = new List<GameObject>();
        public List<GameObject> directionalChunks = new List<GameObject>();
        
        [ReadOnly]
        [Header("Level")]
        public string levelType;
    }
    
#if UNITY_EDITOR
    [CustomEditor(typeof(LevelConfiguration))]
    [CanEditMultipleObjects]
    public class LevelConfigurationEditor : Editor {
        
        private SerializedProperty levelType;
        private List<Type> levelTypes;
        private string[] levelTypesAsString;
        private int popupIndex = 0;
    
        void OnEnable() {
            levelType = serializedObject.FindProperty("levelType");
            levelTypes = GetLevelTypes();
            levelTypesAsString = TypesToString(levelTypes);
            foreach (string levelType in levelTypesAsString) {
                if (levelType.Equals(this.levelType.stringValue)) {
                    break;
                }
                ++popupIndex;
            }
        }

        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            serializedObject.Update();
            popupIndex = EditorGUILayout.Popup("Select Type", popupIndex, levelTypesAsString);
            levelType.stringValue = levelTypes[popupIndex].FullName;
            serializedObject.ApplyModifiedProperties();
        }
            
        private List<Type> GetLevelTypes() {
            var typeBase = typeof(Level);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => p.BaseType == typeBase);
            return types.ToList();
        }

        private string[] TypesToString(List<Type> types) {
            List<string> typesNames = new List<string>();
            types.ForEach(type => typesNames.Add(type.Name));
            return typesNames.ToArray();
        }
    }
#endif
}