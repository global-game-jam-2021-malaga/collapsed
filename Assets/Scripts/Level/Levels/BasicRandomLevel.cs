using System.Collections.Generic;
using Collapsed.Movement;
using UnityEngine;

namespace Collapsed.Level {
    
    public class BasicRandomLevel : Level {

        [SerializeField] 
        protected float secondsToSpawn = 1f;
        [SerializeField] 
        protected float speedToIncreaseEachChunk = 1.05f;

        protected float elapsedTime = 0;
        protected List<KeyValuePair<GameObject, Transform>> enemies = new List<KeyValuePair<GameObject, Transform>>();
        protected PlayerMovement playerMovement;

        protected override Chunk GetRegularChunk() {
            ++numberOfRegularChunks;
            playerMovement.IncreaseSpeed(speedToIncreaseEachChunk);
            return GetRandomChunk(levelConfiguration.regularChunks);
        }
        
        protected override Chunk GetDirectionalChunk() {
            numberOfRegularChunks = 0;
            playerMovement.IncreaseSpeed(speedToIncreaseEachChunk);
            return GetRandomChunk(levelConfiguration.directionalChunks);
        }

        protected override List<KeyValuePair<GameObject, Transform>> GetEnemies() {
            enemies.Clear();
            if (thirdChunk.GetSpawnPoints().Count > 0) {
                int numberOfEnemies = Random.Range(1, thirdChunk.spawnPoints.Count);
                for (byte iterator = 0; iterator < numberOfEnemies; ++iterator) {
                    enemies.Add(new KeyValuePair<GameObject, Transform>(
                        levelConfiguration.enemies[Random.Range(0, levelConfiguration.enemies.Count)],
                        thirdChunk.spawnPoints[Random.Range(0, thirdChunk.spawnPoints.Count)]));
                }
            }
            if (secondChunk.GetSpawnPoints().Count > 0) {
                int numberOfEnemies = Random.Range(1, secondChunk.spawnPoints.Count);
                for (byte iterator = 0; iterator < numberOfEnemies; ++iterator) {
                    enemies.Add(new KeyValuePair<GameObject, Transform>(
                        levelConfiguration.enemies[Random.Range(0, levelConfiguration.enemies.Count)],
                        thirdChunk.spawnPoints[Random.Range(0, secondChunk.spawnPoints.Count)]));
                }
            }
            return enemies;
        }

        protected Chunk GetRandomChunk(List<GameObject> chunks) {
            return chunks[Random.Range(0, chunks.Count)].GetComponent<Chunk>();
        }

        protected override void Start() {
            playerMovement = FindObjectOfType<PlayerMovement>();
            base.Start();
        }
    }
}