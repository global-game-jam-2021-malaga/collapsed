using Cinemachine;

namespace Collapsed.Movement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayerMovement : MonoBehaviour
    {
        public float constSpeedFixedValue = 0.5f;

        public float accelerationSpeed = 0.3f;

        public float minimumSpeedThreshold = -0.1f, maximunSpeedThreshold = 2f;

        public float actualSpeed;

        public float rotationMultiplier = 0.1f;

        public float minimunlateralSpeed = 1f, maximunLateralSpeed = 5f, currentLateralSpeed = 0f;

        public Vector3 nextRotation = new Vector3(0, 90, 0);

        float signOfRotation = 1;

        public GameObject target;

        public CinemachineVirtualCamera cinemachineCamera;

        public float secondsInvulnerability = 3f;

        private CinemachineFramingTransposer cinemachineFramingTransposer;

        Quaternion initialRotation;

        Vector3 movement;

        Rigidbody rigidBodySpaceShip;

        private Vector3 newRotationEuler;
        private Quaternion newRotation;
        private Quaternion directionRotation;
        private int rotationDirection = 0;
        private float newRotationX = 0;
        private bool ableToDamage = true;

        IEnumerator restartConstantMovementCoroutine, returnRotation;


        private void Awake()
        {
            if (target == null)
            {
                target = this.gameObject;
            }
            actualSpeed = constSpeedFixedValue;

            if (target.GetComponent<Rigidbody>())
            {
                rigidBodySpaceShip = target.GetComponent<Rigidbody>();
            }

            initialRotation = target.transform.rotation;
            currentLateralSpeed = minimunlateralSpeed;

            cinemachineFramingTransposer = cinemachineCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
        }

        protected void Move()
        {
            float horizontal = Input.GetAxisRaw("Vertical");
            Vector3 direction = new Vector3(horizontal, 0f, 0f).normalized;
            movement = direction * actualSpeed * Time.deltaTime;
            rigidBodySpaceShip.velocity = direction * actualSpeed * Time.deltaTime;
        }

        protected void Decelerate()
        {
            if (actualSpeed >= minimumSpeedThreshold)
            {
                if (actualSpeed - accelerationSpeed <= 0)
                {
                    actualSpeed = minimumSpeedThreshold;
                }
                else
                {
                    actualSpeed = -accelerationSpeed;
                }
            }
            Move();
        }

        protected void Accelerate()
        {
            if (actualSpeed <= maximunSpeedThreshold)
            {
                actualSpeed = actualSpeed + accelerationSpeed;
            }
            Move();
        }



        protected void LateralMovement()
        {
            Vector3 direction = Vector3.Cross(transform.right, Vector3.up);
            target.transform.position += direction * currentLateralSpeed * Input.GetAxisRaw("Horizontal") * -signOfRotation * Time.deltaTime;
        }



        public void FlowRotation()
        {
            target.transform.rotation = Quaternion.Euler(nextRotation);
            if (nextRotation.y > 0)
            {
                signOfRotation = -1;
                cinemachineFramingTransposer.m_TrackedObjectOffset.x = 20;
            }
            else
            {
                signOfRotation = 1;
                if (nextRotation.y < 0) {
                    cinemachineFramingTransposer.m_TrackedObjectOffset.x = 20;
                }
                else {
                    cinemachineFramingTransposer.m_TrackedObjectOffset.x = 35;
                }
            }
        }


        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.W))
            {
                if (restartConstantMovementCoroutine == null)
                {
                    restartConstantMovementCoroutine = RigidbodyMovement();
                    StartCoroutine(restartConstantMovementCoroutine);
                }
            }


            if (Input.GetKey(KeyCode.W))
            {
                if (restartConstantMovementCoroutine != null)
                {
                    StopCoroutine(restartConstantMovementCoroutine);
                    restartConstantMovementCoroutine = null;
                }

                Accelerate();
            }



            if (Input.GetKey(KeyCode.S))
            {
                if (restartConstantMovementCoroutine != null)
                {
                    StopCoroutine(restartConstantMovementCoroutine);
                    restartConstantMovementCoroutine = null;
                }

                Decelerate();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                //ChangeRotation
                FlowRotation();
            }


            StaticMovement();

            // Rotation
            if (Input.GetKey(KeyCode.A))
            {
                newRotationX = 90f;
                rotationDirection = 1;
                if (currentLateralSpeed < maximunLateralSpeed)
                {
                    currentLateralSpeed += constSpeedFixedValue;
                }
                LateralMovement();
            }
            else if (Input.GetKey(KeyCode.D))
            {
                newRotationX = -90f;
                rotationDirection = -1;
                if (currentLateralSpeed < maximunLateralSpeed)
                {
                    currentLateralSpeed += constSpeedFixedValue;
                }
                LateralMovement();
            }
            else if (newRotationX != 0f)
            {
                newRotationX = 0f;
                rotationDirection *= -1;
            }
            newRotationEuler.Set(newRotationX, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
            newRotation.eulerAngles = newRotationEuler;
            if (!ReachRotation(transform.rotation, newRotation))
            {
                directionRotation.eulerAngles = Vector3.right * rotationDirection * rotationMultiplier;
                transform.rotation *= directionRotation;

            }
        }

        private bool ReachRotation(Quaternion rotation1, Quaternion rotation2)
        {
            return Mathf.Approximately(Quaternion.Dot(rotation1, rotation2), 1.0f);
        }

        IEnumerator RigidbodyMovement()
        {
            float time = 0;
            float duration = 5f;
            while (time < duration)
            {
                rigidBodySpaceShip.velocity = transform.forward * actualSpeed * Time.deltaTime;
                actualSpeed = Mathf.Lerp(actualSpeed, constSpeedFixedValue, time / duration);
                time += Time.deltaTime;
                yield return null;
            }

            actualSpeed = constSpeedFixedValue;
            restartConstantMovementCoroutine = null;
        }

        private void StaticMovement()
        {
            Vector3 staticMovement = new Vector3(1f, 0, 0) * actualSpeed * Time.deltaTime;

            target.transform.Translate(staticMovement, Space.Self);
        }


        public void Damage()
        {
            if (ableToDamage)
            {
                Master.Instance.addDamage(gameObject);
                ableToDamage = false;
                StartCoroutine(Invulnerability());
            }
        }

        public void Death()
        {
            Master.Instance.finish(gameObject);
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals("Wall"))
            {
                Death();
            }

            if (other.tag.Equals("Enemy"))
            {
                Damage();
            }
        }
        protected IEnumerator Invulnerability()
        {
            yield return new WaitForSeconds(secondsInvulnerability);
            ableToDamage = true;
        }

        public void IncreaseSpeed(float multiplier)
        {
            constSpeedFixedValue *= multiplier;
            accelerationSpeed *= multiplier;
            maximunSpeedThreshold *= multiplier;
            minimumSpeedThreshold *= multiplier;
            maximunLateralSpeed *= multiplier;
            minimunlateralSpeed *= multiplier;
        }
    }
}