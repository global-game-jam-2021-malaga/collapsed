using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Collapsed.Movement;

public class ReflexTurn : MonoBehaviour
{
    
    public Vector3 rotation;


    private void OnTriggerEnter(Collider other) {
        if(other.tag.Equals("Player"))
        {
            other.GetComponent<PlayerMovement>().nextRotation = rotation;
        }
    }
}
