using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OpacityController : MonoBehaviour
{
    // Start is called before the first frame update
    public Image image;
    public GameObject camera;
    public GameObject Building;
    public GameObject ramp;
    void Start()
    {
        //other.renderer.material.color.a = 1.0; // fully opaque
        
        image = GetComponent<Image>();
        var tempColor = image.color;
        tempColor.a = 1;
        image.color = tempColor;
        Invoke("startDecrease",5f);
        Invoke("changeScene",70f);
    }

    private void startDecrease(){
        StartCoroutine(decrease());
    }

    private void changeScene(){
        SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
    }
    private IEnumerator decrease()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            var tempColor = image.color;
            tempColor.a = tempColor.a-0.1f;
            image.color = tempColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape)){
            SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
        }
    }
}
