using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    public Transform cameraTransform;
    public bool infiniteHorizontal = true;
    public bool infiniteVertical = true;
    protected Vector3 lastCameraPosition;
    public Vector2 parallaxEffectMultiplier = new Vector2(0.1f, 0.5f);
    private float textureUnitSizeX;
    private float textureUnitSizeY;


    private void Start()
    {
        if (cameraTransform == null)
        {
            cameraTransform = Camera.main.transform;
        }
        lastCameraPosition = cameraTransform.position;
        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        Texture2D texture = sprite.texture;
        textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
        textureUnitSizeY = texture.height / sprite.pixelsPerUnit;

    }

    private void LateUpdate()
    {
        Vector3 deltaMovement = cameraTransform.position - lastCameraPosition;
        transform.position += new Vector3(deltaMovement.x * parallaxEffectMultiplier.x, 0, deltaMovement.z * parallaxEffectMultiplier.y);
        lastCameraPosition = cameraTransform.position;

        if (infiniteHorizontal)
        {
            if (Mathf.Abs(cameraTransform.position.x - transform.position.x) >= textureUnitSizeX)
            {
                float offsetpostionX = (cameraTransform.position.x - transform.position.x) % textureUnitSizeX;
                transform.position = new Vector3(cameraTransform.position.x + offsetpostionX, 0, transform.position.z);
            }
        }

        if (infiniteVertical)
        {
            if (Mathf.Abs(cameraTransform.position.z - transform.position.z) >= textureUnitSizeY)
            {
                float offsetpostionY = (cameraTransform.position.z - transform.position.z) % textureUnitSizeY;
                transform.position = new Vector3(transform.position.x, 0, cameraTransform.position.z + offsetpostionY);
            }
        }
    }
}
