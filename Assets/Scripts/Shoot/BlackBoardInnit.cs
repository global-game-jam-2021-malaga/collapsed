using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class BlackBoardInnit : MonoBehaviour
{
   
    Blackboard blackboard;

   private void Awake() {
       
       blackboard = GetComponent<Blackboard>();
       blackboard.AddVariable<GameObject>("Target", GameObject.FindGameObjectWithTag("Player"));
   }
}
