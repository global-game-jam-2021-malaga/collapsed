using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Collapsed.Movement;

public class Bullet : MonoBehaviour
{
    public float speed = 3f;
    public float lifeTime = 5f;

    public Transform explosionPrefab;

    Rigidbody rb;
    private Transform shootDirection;

    string shooterTag;
    public void Direction(GameObject gunPosition, string tag)
    {
        this.shootDirection = gunPosition.transform;
        Destroy(gameObject, lifeTime);
        rb = GetComponent<Rigidbody>();
        rb.AddForce(shootDirection.up * speed, ForceMode.Impulse);
        shooterTag = tag;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Enemy") || other.gameObject.tag.Equals("Player"))
        {


            if (other.gameObject.tag.Equals("Player") && !shooterTag.Equals("Player"))
            {
                BulletDespawn();
                other.GetComponent<PlayerMovement>().Damage();
            }

            if (other.gameObject.tag.Equals("Enemy") && !shooterTag.Equals("Enemy"))
            {
                if (other.GetComponent<BulletDie>())
                {
                    BulletDespawn();
                    BulletDie die = other.GetComponent<BulletDie>();
                    die.AddScore();
                    die.EnemyDeath();
                }
            }
        }

        if (other.gameObject.tag.Equals("CameraCollider") || other.gameObject.tag.Equals("CameraColliderBack")) {
            Destroy(gameObject);
        }
    }

    protected void BulletDespawn()
    {
        this.GetComponent<Renderer>().enabled = false;
        rb.isKinematic = true;
        Transform explotionGameobject = Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
        explotionGameobject.parent = this.transform;
    }
}
