using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDie : MonoBehaviour
{

    public Transform explotionPrefab;

    public int score = 150;

    public void EnemyDeath()
    {
        Transform explotionGameobject = Instantiate(explotionPrefab,this.gameObject.transform.position,explotionPrefab.rotation);
        explotionGameobject.localScale = new Vector3(15,15,15);
        this.gameObject.GetComponentInChildren<Renderer>().enabled =false;
        this.gameObject.GetComponent<Collider>().enabled = false;
        Destroy(gameObject, 2f);
    }

    public void AddScore()
    {
        Master.Instance.addScore(score);
    }
}
