using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShooterEntity : MonoBehaviour
{
    public event EventHandler<ShootEventArgs> onShoot;

    public bool keyShootEnable = false;

    public bool periodicShoot = false;

    public float repeatTime = 3f;

    public KeyCode keyToShoot;


    private void Start()
    {
        if (periodicShoot)
        {
            InvokeRepeating("Shoot", 1, repeatTime);
        }
    }

    private void Update()
    {
        if (keyShootEnable)
        {
            if (Input.GetKeyDown(keyToShoot))
            {
                onShoot!.Invoke(gameObject, new ShootEventArgs(transform.GetChild(0).gameObject, transform.position));
            }
        }
    }

    public void Shoot()
    {
        onShoot?.Invoke(gameObject, new ShootEventArgs(transform.GetChild(0).gameObject, transform.position));
    }
}




public class ShootEventArgs : EventArgs
{
    public GameObject endGunPosition;

    public Vector3 playerPosition;

    public ShootEventArgs(GameObject sPos, Vector3 playerPos)
    {
        this.endGunPosition = sPos;
        this.playerPosition = playerPos;
    }
}
