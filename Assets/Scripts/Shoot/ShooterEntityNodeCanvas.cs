using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using ParadoxNotion.Design;

[Category("Collapsed")]
public class ShooterEntityNodeCanvas : ActionTask
{
    ShooterEntity shooter;

    protected override string OnInit() {
        
        shooter = agent.GetComponent<ShooterEntity>();
        return null;
    }
    
    protected override void OnExecute()
    {
        shooter.Shoot();
        EndAction(true);
    }
}
