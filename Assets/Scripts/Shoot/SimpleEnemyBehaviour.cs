using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemyBehaviour : MonoBehaviour
{
    public Transform projectile;

    protected ShooterEntity shooter;

    private void Start() {
        if(shooter == null)
        {
            shooter = GetComponent<ShooterEntity>();
            shooter.onShoot += ShootProjectile;
        }
    }

    private void ShootProjectile(object sender, ShootEventArgs e)
    {
        Transform bullet =Instantiate(projectile,e.endGunPosition.transform.position, e.endGunPosition.transform.rotation);
        Vector3 direction = e.endGunPosition.transform.position;
        bullet.GetComponent<Bullet>().Direction(e.endGunPosition, this.gameObject.tag);
    }


    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag.Equals("CameraColliderBack")) {
            BulletDie bulletDie = GetComponent<BulletDie>();
            if (bulletDie) {
                bulletDie.EnemyDeath();
                bulletDie.AddScore();
            }
        }

        if (other.gameObject.tag.Equals("Wall")) {
            BulletDie bulletDie = GetComponent<BulletDie>();
            if (bulletDie) {
                bulletDie.EnemyDeath();
            }
        }
    }
}
