using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalChunk : MonoBehaviour {
    
    public DoorManager doorManager;

    private void finish(){
        doorManager.open();
        Master.Instance.victoryFinish();
    }

    private void OnTriggerEnter(Collider other)
    {
        finish();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
