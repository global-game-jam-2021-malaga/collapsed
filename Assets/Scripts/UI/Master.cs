using System.Collections;
using System.Collections.Generic;
using Collapsed.Movement;
using UnityEngine;
using UnityEngine.UI;

public class Master : MonoBehaviour
{
    public Text scoreValue;
    public GameObject Life1;
    public GameObject Life2;
    public GameObject Life3;
    public GameObject FinishPanel;
    public GameObject ScorePanel;
    public GameObject PlayerPanel;
    public Text FinishLabel;
    public Text YourScore;
    public Animator scoreAnimator;
    private bool scoreAnimatorPlaying;
    public int life = 4;

    private void Awake() {
        if(Instance != null && Instance != this)
        {
            //Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
 
    void Start()
    {
        FinishPanel.SetActive(false);
    }

    public static Master Instance { get; private set;}

    private Master() { }

    private void disableScoreAnimation(){
        scoreAnimator.SetBool("play", false);
        scoreAnimatorPlaying = false;
    }
    public string addScore(long value){
        value = value + int.Parse(scoreValue.text);
        char[] valuec = ("" + value).ToCharArray();
        int dif = 9 - valuec.Length;
        scoreValue.text= "" ;
        for(int i = 0; i<dif; i++){scoreValue.text += "0";}
        scoreValue.text += "" + value;
        if(!scoreAnimatorPlaying){
            scoreAnimatorPlaying=true;
            scoreAnimator.SetBool("play", true);
            Invoke("disableScoreAnimation",0.2f);
        }       
        return scoreValue.text;
    }
    
    public string getScore(){
        return scoreValue.text;
    }

    public int getLife(){
        return life;
    }

    public void addDamage(GameObject player){
        life--;
        switch(life){
            case 3:
                Life3.SetActive(false);
            break;
            case 2:
                Life3.SetActive(false);
                Life2.SetActive(false);
            break;
            case 1:
                Life3.SetActive(false);
                Life2.SetActive(false);
                Life1.SetActive(false);
            break;
            case 0:
                finish(player);
            break;
        }
    }

    public void finish(GameObject player){
        for (byte iterator = 0; iterator < player.transform.childCount; ++iterator) {
            Destroy(player.transform.GetChild(iterator).gameObject);
        }
        Destroy(player);
        
        FinishLabel.text = "-- [ Game Over ] --";
        YourScore.text = "score: " + getScore();
        FinishPanel.SetActive(true);
        PlayerPanel.SetActive(false);
        ScorePanel.SetActive(false);
        //Destroy all enemies on scene?
    }

    public void victoryFinish(){
        FinishLabel.text = "-- [ Victory ] --";
        YourScore.text = "score: " + getScore();
        FinishPanel.SetActive(true);
        PlayerPanel.SetActive(false);
        ScorePanel.SetActive(false);
        Debug.Log("Finishedd");
        //Destroy all enemies on scene?
    }

}
