using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class OnMouseOverUI : MonoBehaviour
{
    public Text text;
    public Color color;

    public UnityEvent onMouseDown;
    void OnMouseOver()
    {
        text.fontSize = 130;
        text.color = Color.yellow;
    }

    void OnMouseExit()
    {
        text.fontSize = 95;
        text.color = Color.white;
    }

    private void OnMouseDown() {
        onMouseDown?.Invoke();
    }
}
