using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreIntro : MonoBehaviour
{
    private bool go = false;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("reActivate",3f);
    }

    private void OnTriggerEnter(Collider other)
    {
        go = false;
        GetComponent<Collider>().enabled = false;
        Invoke("reActivate",2f);
    }

    void reActivate(){
        go = true;
    }
    // Update is called once per frame
    void Update()
    {
        if(go)
        transform.position += Vector3.up * Time.deltaTime * 10f;
    }
}
