using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneManager.GetActiveScene().name=="GameScene" || SceneManager.GetActiveScene().name=="MainMenuScene"){
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if(SceneManager.GetActiveScene().name=="MainMenuScene"){
                    SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
                }
                if(SceneManager.GetActiveScene().name=="GameScene"){
                    SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
                }
            }
        }
    }
}
