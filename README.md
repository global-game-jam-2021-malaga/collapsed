![](Images/CollapsedBannerGreen.png)

​You are a space traveler that needs to return to your planet. Fight a horde of enemies to reach your home!!

## Play & Download

You can play and download the game on its Itchio page: https://hacayard.itch.io/collapsed

## Controls

Use WASD to move the spaceship: \
-> W to increase velocity \
-> S to decrease velocity \
-> A to move to the left \
-> D to move to the right

Use SPACE to turn right or left when the game allows you.

## Developers

This game was developed by:

Art:

**Miguel Ángel García Vázquez** -> miguelagvazquez@gmail.com

Programming:

**Tasio Candón Hernández** -> [@HyenaCalibalims](https://twitter.com/HyenaCalibalims) / [LinkedIn](https://www.linkedin.com/in/tasio-cand%C3%B3n-hern%C3%A1ndez-74707814a/) / TasCanHer@gmail.com​​​

**Javier Osuna Herrera** -> [@javosuher](https://twitter.com/javosuher) / [LinkedIn](https://www.linkedin.com/in/javierosunaherrera/) / javier.osunaherrera@gmail.com
